#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  webcrawler.py
#  
#  Felipe Aguiar Correa <facorrea@inf.ufpel.edu.br>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import time
import socket
import sys
import re
import threading
import ssl
import pprint

class InternetGraph:	
	def __init__(self, name):
		self.graphID = name
		self.vortexList = {}
		self.neighborsList = {}
		
	def addWebsite(self, url):
		self.vortexList[url] = True
	
	def removeWebsite(self, url):
		for vortex in self.neighborsList[url]:
			self.removeWebsite(url, vortex)
		del self.neighborsList[url]
		del self.vortexList[url]
		
	#Oriented Graph, since a website can link to one website that don't hotlink it back	
	def addConnection(self, firstURL, secURL):
		if not firstURL in self.neighborsList:
			self.neighborsList[firstURL] = []
		self.neighborsList[firstURL].append(secURL)
	
	def deleteConnection(self, firstURL, secURL):
		self.neighborsList(firstURL).remove(secURL)
	
	def getLinks(self, url):
		if url in self.neighborsList:
			return self.neighborsList[url]
		else:
			return []
	
	def getWebsitesList(self):
		return self.vortexList.keys()	

class Webcrawler():
	def __init__(self, url, prof):
		self.url = url
		self.prof = prof
		self.graph = InternetGraph("Internet Graph")
		self.run()
		
	def run(self):
		if self.prof==0:
			return []
			
		htmlcode = ''
		temp1024bytes = '-'
		PORT = 80
		initialTime = time.time()
		self.urlParser()
		self.graph.addWebsite(self.url)
		DIR = self.parseDirectory()
		HOST = self.parseHost()
		
		print "Connecting to - " + HOST + " On the directory:   " + DIR
		
		con = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		con.settimeout(60) #Connection Timeout
		try:
			con.connect((HOST,PORT))
		except:
			print "Timed Out on Socket (or wrong link, who knows...)"
			return []
		
		try:
			con.send("GET %s HTTP/1.1\r\nHost: %s\r\n\r\n" %(DIR,HOST)) 
		except:
			print "Cannot send HTTP Request. :("
			return []
		
		while(len(temp1024bytes)!=0):
			try:
				temp1024bytes = con.recv(1024)
				htmlcode=htmlcode+temp1024bytes
			except:
				print "Connection Reset by Peer. Booooh."
				break
		con.close()
		foundUrlList = self.urlFinder(htmlcode)			
		foundUrlList = self.removeDuplicateLink(foundUrlList)
		print foundUrlList
		for url in foundUrlList:
			self.graph.addConnection(self.url, url)
		
		print self.graph.getWebsitesList()
		print self.graph.getLinks(self.url)
		
		for i in foundUrlList:
			Webcrawler(i, self.prof-1)
	
	def checkHTTPS(self, url): #Checa HTTPS
		if "https://" in url:
			return True
		else:
			return False
		
	def urlParser(self):
		#Remove "http://" and "https://" from the URL
		if "https" in self.url:
			self.url=self.url.split("https://")[-1]
		else:
			self.url=self.url.split("http://")[-1]
		return self.url
			
	def parseHost(self):
		#Extracts HOST from URL
		if self.url.split("/")[0] == self.url:
			HOST = self.url
		else:
			HOST = self.url.split("/",1)[0]
		return HOST

	def parseDirectory(self):
		#Extracts Directory from URL
		if self.url.split("/")[0] == self.url:
			DIR = "/"
		else:
			DIR = "/" + self.url.split("/", 1)[1]
		
		return DIR
		
	def urlFinder(self, html):
		self.urls = re.findall(r'a href=[\'"]?([^\'" >]+)', html)
		return self.urls

	def removeDuplicateLink(self, foundUrlList):
		auxiliar = []
		for i in range (0, len(foundUrlList)):
			if not(foundUrlList[i] in auxiliar):
				auxiliar.append(foundUrlList[i])
				
		return auxiliar
	
		
def main():
	url = "http://www.vglist.net/about/"
	prof = 2
	webcrawler = Webcrawler(url, prof)
	
	return 0

if __name__ == '__main__':
	sys.setrecursionlimit(100000000)
	main()

